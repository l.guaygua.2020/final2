import sys
from transforms import *
from images import read_img, write_img, create_blank

def main():
    # i=0 transform
    image = read_img(sys.argv[1]) #archivo (foto)
    image_trans = image
    multi_trans = sys.argv[2:]
    i = 0 # porque lo siguente que va a coger transformación (función)

    while i < len(multi_trans):

        if multi_trans[i] == "rotate":
            if len(multi_trans) > i + 1 and multi_trans[i + 1] == 'left':
                image_trans = rotate(image, 'left')
                salto = 2 #contador
            else:
                image_trans = rotate(image, 'right')
                salto = 1
        elif multi_trans[i] == "mirror":
            image_trans = mirror(image)
            salto = 1
        elif multi_trans[i] == "blur":
            image_trans = blur(image)
            salto = 1
        elif multi_trans[i] == "grayscale":
            image_trans = grayscale(image)
            salto = 1
        elif multi_trans[i] == "change_color":
            if len(multi_trans) >= i + 3:
                original = [tuple(map(int, color.split(','))) for color in multi_trans[i + 1].split(':')]
                change = [tuple(map(int, color.split(','))) for color in multi_trans[i + 2].split(':')]
                image_trans = change_colors(image_trans, original, change)
                salto = 3
            else:
                print("Usage: python3 transform_multi.py <image_file> change_color <original_colors>:<new_colors>")
                return

        elif multi_trans[i] == "shift":
            image_trans = shift(image_trans, int(multi_trans[i + 1]), int(multi_trans[i + 2]))
            salto = 3
        elif multi_trans[i] == "crop":
            image_trans = crop(image, int(multi_trans[i+1]), int(multi_trans[i+2]), int(multi_trans[i+3]), int(multi_trans[i+4]))
            salto = 5
        elif multi_trans[i] == "filter":
            image_trans = filter(image, float(multi_trans[i+1]), float(multi_trans[i+2]), float(multi_trans[i+3]))

            salto = 4
        else:
            print("La función no existe")
            salto = 1

        i += salto
        image = image_trans

    file_name = sys.argv[1].replace(".", "_trans.")
    write_img(image_trans, file_name)

if __name__ == "__main__":
    main()