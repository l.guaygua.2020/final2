from images import read_img, write_img, create_blank
import sys
from transforms import *
#from transform import mirror, blur, grayscale

def main():
    if len(sys.argv) != 3:
        print("Usage: python transform_simple.py <input_file> <transformation>")
    else:
        image = read_img(sys.argv[1])
        if sys.argv[2] == "mirror":
            image_trans = mirror(image)
        elif sys.argv[2] == "blur":
            image_trans = blur(image)
        elif sys.argv[2] == "grayscale":
            image_trans = grayscale(image)
        else:
            print("Invalid transformation")
            sys.exit(1)

        new_filename = sys.argv[1].replace(".", "_trans.")
        write_img(image_trans, new_filename)

if __name__ == '__main__':
    main()